package id.co.pangansari.manajemenaset;

import java.util.Map;

import id.co.pangansari.manajemenaset.GetAsetAdapter;

import id.co.pangansari.manajemenaset.model.LoginResultModel;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("list")
    Call<GetAsetAdapter> getAset(@Query("itemCode") String itemCode);

    @POST("login")
    Call<ResponseBody> checkLogin(@Query("userName") String userName, @Query("password") String password);

    @Headers("Content-Type: application/json")
    @POST("jsonrequest/")
    Call<LoginResultModel> testLogin(@Body String body);

    /*
    TODO Retrofit Interface - upload image
    */
    @Multipart
    @POST("uploadImage")
    Call<ResponseBody> uploadImage(@Part MultipartBody.Part photo,
                                   @PartMap Map<String, RequestBody> text);
}
