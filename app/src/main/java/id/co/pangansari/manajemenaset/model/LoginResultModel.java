package id.co.pangansari.manajemenaset.model;

import com.google.gson.annotations.SerializedName;

public class LoginResultModel {
    @SerializedName("status")
    String status;

    public LoginResultModel(String status){
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
