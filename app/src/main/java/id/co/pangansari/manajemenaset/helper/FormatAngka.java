package id.co.pangansari.manajemenaset.helper;

import java.text.NumberFormat;

public class FormatAngka {
    public static String rupiah(int nom){
        String hasil = "Rp " + NumberFormat.getInstance().format(nom) + ",-";
        return hasil;
    }
}
