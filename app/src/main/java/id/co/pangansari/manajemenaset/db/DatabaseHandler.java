package id.co.pangansari.manajemenaset.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import id.co.pangansari.manajemenaset.AsetModel;

public class DatabaseHandler  extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "AssetManager";
    private static final String TABLE_ASSET = "tb_asset";
    public static final String KEY_ID = "id";
    public static final String KEY_ITEM_CODE = "itemCode";
    public static final String KEY_ITEM_NAME = "itemName";
    public static final String KEY_ITEM_QTY = "itemQty";
    public static final String KEY_ITEM_UNIT = "itemUnit";
    public static final String KEY_ITEM_PRICE = "itemPrice";
    public static final String KEY_ITEM_DESC = "itemDesc";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ASSET_TABLE = "CREATE TABLE " + TABLE_ASSET + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_ITEM_CODE + " TEXT,"
                + KEY_ITEM_NAME + " TEXT,"
                + KEY_ITEM_QTY + " INTEGER,"
                + KEY_ITEM_UNIT + " TEXT,"
                + KEY_ITEM_PRICE + " INTEGER,"
                + KEY_ITEM_DESC + " TEXT "
                + ")";
        db.execSQL(CREATE_ASSET_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ASSET);

        // Create tables again
        onCreate(db);
    }

    public void addAsset(AsetModel asset) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ITEM_CODE, asset.getCode());
        values.put(KEY_ITEM_NAME, asset.getName());
        values.put(KEY_ITEM_QTY, asset.getQty());
        values.put(KEY_ITEM_UNIT, asset.getUnit());
        values.put(KEY_ITEM_PRICE, asset.getPrice());
        values.put(KEY_ITEM_DESC, asset.getDesc());

        // Inserting Row
        db.insert(TABLE_ASSET, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    // code to get the single contact
    public AsetModel getAsset(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_ASSET, new String[] { KEY_ID,
                        KEY_ITEM_CODE, KEY_ITEM_NAME, KEY_ITEM_QTY, KEY_ITEM_UNIT,
                        KEY_ITEM_PRICE, KEY_ITEM_DESC}, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        AsetModel assetresult = new AsetModel(
                cursor.getString(1),
                cursor.getString(2),
                Integer.parseInt(cursor.getString(3)),
                cursor.getString(4),
                Integer.parseInt(cursor.getString(5)),
                cursor.getString(6)
        );
        // return contact
        return assetresult;
    }

    // code to get all contacts in a list view
    public List<AsetModel> getAllAsset() {
        List<AsetModel> assetList = new ArrayList<AsetModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ASSET;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                AsetModel contact = new AsetModel(
                        cursor.getString(1),
                        cursor.getString(2),
                        Integer.parseInt(cursor.getString(3)),
                        cursor.getString(4),
                        Integer.parseInt(cursor.getString(5)),
                        cursor.getString(6));

                // Adding contact to list
                assetList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        return assetList;
    }

    // code to update the single contact
    /*public int updateContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, contact.getName());
        values.put(KEY_PH_NO, contact.getPhoneNumber());

        // updating row
        return db.update(TABLE_ASSET, values, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });
    }

    // Deleting single contact
    public void deleteContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ASSET, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });
        db.close();
    }

    // Getting contacts Count
    public int getContactsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_ASSET;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }*/
}
