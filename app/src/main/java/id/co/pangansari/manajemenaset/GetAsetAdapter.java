package id.co.pangansari.manajemenaset;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class GetAsetAdapter {
    @SerializedName("status")
    String status;
    @SerializedName("result")
    List<AsetModel> listDataAset;
    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public List<AsetModel> getListDataAset() {
        return listDataAset;
    }
    public void setListDataAset(List<AsetModel> listDataAset) {
        this.listDataAset = listDataAset;
    }
}
