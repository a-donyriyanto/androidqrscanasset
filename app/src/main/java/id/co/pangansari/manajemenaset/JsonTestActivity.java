package id.co.pangansari.manajemenaset;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import id.co.pangansari.manajemenaset.model.LoginResultModel;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JsonTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json_test);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("username", "adminabc");
            paramObject.put("password", "admin123");
            Log.d("DONY", ""+paramObject.toString());

            Call<LoginResultModel> userCall = apiInterface.testLogin(paramObject.toString());
            userCall.enqueue(new Callback<LoginResultModel>(){

                @Override
                public void onResponse(Call<LoginResultModel> call, Response<LoginResultModel> response) {
                    Log.d("DONY", ""+response.raw().toString());
                    if(response.isSuccessful()) {
                        //response.body();
                        String status = response.body().getStatus();
                        Log.d("DONY", status);
                    }
                    /*if(response.code()==200){
                        Log.d("DONY","login berhasil");
                    } else {
                        Log.d("DONY","login gagal");
                    }*/
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.d("DONY", t.getMessage());
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
