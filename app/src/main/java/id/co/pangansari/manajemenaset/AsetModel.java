package id.co.pangansari.manajemenaset;

import com.google.gson.annotations.SerializedName;
import id.co.pangansari.manajemenaset.helper.*;
import java.text.NumberFormat;

public class AsetModel {
    @SerializedName("itemCode")
    String itemCode;
    @SerializedName("itemName")
    String itemName;
    @SerializedName("itemQty")
    int itemQty;
    @SerializedName("itemUnit")
    String itemUnit;
    @SerializedName("itemPrice")
    int itemPrice;
    @SerializedName("itemDesc")
    String itemDesc;

    public AsetModel(String iCode, String iName, int iQty, String iUnit, int iPrice,String itemDesc) {
        itemCode = iCode;
        itemName = iName;
        itemQty = iQty;
        itemUnit = iUnit;
        itemPrice = iPrice;
    }

    public String getCode() {
        return itemCode;
    }

    public String getName() {
        return itemName;
    }

    public String getQty() {
        return ""+itemQty;
    }

    public String getUnit() {
        return itemUnit;
    }

    public int getPrice() {
        return itemPrice;
    }

    public String getDesc() {
        return itemDesc;
    }
}
