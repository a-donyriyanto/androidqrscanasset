package id.co.pangansari.manajemenaset;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import java.util.List;

import id.co.pangansari.manajemenaset.db.DatabaseHandler;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<AsetModel> dataModels;
    ListView listView;
    private static AsetAdapter adapter;
    ApiInterface mApiInterface;
    public static MainActivity ma;
    String itemCodeScanned = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        itemCodeScanned = intent.getStringExtra(ScanActivity.EXTRA_MESSAGE_CODE);
        String name = intent.getStringExtra(ScanActivity.EXTRA_MESSAGE_NAME);

        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        ma=this;
        refresh(itemCodeScanned);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                refresh(itemCodeScanned);
            }
        });



        DatabaseHandler db = new DatabaseHandler(this);

        // Inserting Contacts
        Log.d("Insert: ", "Inserting data..");
        db.addAsset(new AsetModel("X001", "ATK", 1, "set", 20000, "Alat tulis set" ));

        Log.d("Reading: ", "Reading all asset..");
        List<AsetModel> assets = db.getAllAsset();

        for (AsetModel rowasset : assets) {
            String log = "Code: " + rowasset.getCode() + "," +
                         "Name: " + rowasset.getName() + "," +
                         "Qty: " + rowasset.getQty() + "," +
                         "Unit: " + rowasset.getUnit() + "," +
                         "Price: " + rowasset.getPrice() + "," +
                         "Desc: " + rowasset.getDesc();
            // Writing Contacts to log
            Log.d("Row data: ", log);
        }
    }

    public void refresh(){
        this.refresh("");
    }

    public void refresh(String itemCode) {
        Call<GetAsetAdapter> asetCall = mApiInterface.getAset(itemCode);
        asetCall.enqueue(new Callback<GetAsetAdapter>() {
            @Override
            public void onResponse(Call<GetAsetAdapter> call, Response<GetAsetAdapter>
                    response) {
                Log.d("Retrofit Get", response.raw().toString());
                List<AsetModel> asetList = response.body().getListDataAset();
                Log.d("Retrofit Get", "Jumlah data aset: " +
                        String.valueOf(asetList.size()));

                listView=(ListView)findViewById(R.id.list);
                dataModels= new ArrayList<>();
                for(int i=0;i<asetList.size();i++){
                    dataModels.add(new AsetModel(asetList.get(i).itemCode,asetList.get(i).itemName, asetList.get(i).itemQty, asetList.get(i).itemUnit,asetList.get(i).itemPrice, asetList.get(i).itemDesc));
                }
                adapter= new AsetAdapter(dataModels,getApplicationContext());

                listView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<GetAsetAdapter> call, Throwable t) {
                Log.e("Retrofit Get", t.toString());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
